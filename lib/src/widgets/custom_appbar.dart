import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  final String texto;
  const CustomAppBar({@required this.texto});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
          top: true,
          bottom: false,
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Container(
          margin: EdgeInsets.only(top: 30),
          width: double.infinity,
          child: Row(
            children: [
              Text(texto,style: TextStyle(fontWeight: FontWeight.w700,fontSize: 30)),
              Spacer(),
              Icon(Icons.search,size: 30)
            ],
          ),
        ),
      ),
    );
  }
}